import React from "react";
import Constants from "expo-constants";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";

const NavigationButton = ({ title, setStack, to, setScreen }) => (
  <TouchableOpacity
    style={styles.button}
    onPress={() => {
      setStack(to);
      setScreen("navigation");
    }}
  >
    <Text style={styles.buttonTitle}>{title}</Text>
  </TouchableOpacity>
);

export default function Main({ setStack, setScreen }) {
  return (
    <View style={styles.container}>
      <NavigationButton
        title="Equipments"
        setStack={setStack}
        to={"equipments"}
        setScreen={setScreen}
      />
      <NavigationButton
        title="Manufacturers"
        setStack={setStack}
        to={"manufacturers"}
        setScreen={setScreen}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: Constants.statusBarHeight,
    alignItems: "center",
    justifyContent: "center",
  },
  button: {
    width: "90%",
    height: 60,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "gray",
    borderTopLeftRadius: 7,
    borderTopRightRadius: 7,
    borderBottomLeftRadius: 7,
    borderBottomRightRadius: 7,
    marginTop: 30,
  },
  buttonTitle: {
    color: "white",
    letterSpacing: 2,
    fontWeight: "700",
  },
});
