import React from "react";
import { ScrollView, View, StyleSheet, Dimensions } from "react-native";
import { useNavigation } from "@react-navigation/native";

import TextInput from "../components/formItems/textInput";
import DateTimePicker from "../components/formItems/dateTimePicker";
import Slider from "../components/formItems/slider";
import Button from "../components/formItems/button";
import Carousel from "../components/formItems/carousel";

import { validateEquipmentRequest as validateForm } from "../utils/formValidation";

import { apps } from "../mockData/Apps.json";

const { height, width } = Dimensions.get("window");

export default function EquipmentRequest({
  initialData,
  appIdx,
  handleSubmit,
}) {
  const [data, setData] = React.useState({ ...initialData });
  const [layout, setLayout] = React.useState([]);
  const navigation = useNavigation();
  //* =========================================================
  //* ==================  SET INITIAL LAYOUT ==================
  //* =========================================================
  React.useEffect(() => {
    const getLayout = () => {
      let fields = [],
        tempFields = [],
        currentRow = 0;
      apps[appIdx].layout.forEach((ele) => {
        if (currentRow === ele.position.row) {
          tempFields.push(ele);
        } else {
          fields.push([...tempFields]);
          tempFields.splice(0, tempFields.length);
          tempFields.push(ele);
          currentRow += 1;
        }
      });
      setLayout([...fields, tempFields]);
    };
    getLayout();
    return () => setData({ ...initialData });
  }, []);

  //* =========================================================
  //* ==============  SELECTOR FORM ELEMENTS ==================
  //* =========================================================
  const selector = (field, length) => {
    const items = {
      textbox: (
        <TextInput
          placeholder={field.field}
          length={length}
          data={data}
          setData={setData}
        />
      ),
      datePicker: (
        <DateTimePicker length={length} data={data} setData={setData} />
      ),
      slider: (
        <Slider
          placeholder={field.field}
          length={length}
          {...field}
          data={data}
          setData={setData}
          type={"Quantity"}
        />
      ),
      "carousel:Equipment.Image": <Carousel data={data} setData={setData} />,
    };
    return items[field.display];
  };
  //* =========================================================
  //* ===================  VALIDATE FORM  =====================
  //* =========================================================
  const validateAndSubmit = () => {
    const result = validateForm(data, appIdx);
    if (!result[0]) return alert(result[1]);
    handleSubmit(data, navigation);
    setData({...initialData})
  };
  return (
    <View style={styles.container}>
      <ScrollView>
        {layout.map((array, idx) => {
          return (
            <View style={styles.row} key={idx}>
              {array.map((field) => (
                <View key={field.field}>{selector(field, array.length)}</View>
              ))}
            </View>
          );
        })}
        <Button title="Submit" action={validateAndSubmit} />
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    width,
  },
  row: {
    width,
    flexDirection: "row",
    height: height / 10,
    marginTop: 30,
  },
  higherRow: {
    height: height / 7,
  },
});
