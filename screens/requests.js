import React from "react";
import { FlatList, SafeAreaView, StyleSheet } from "react-native";

import RequestCard from "../components/cards/requestCard";

export default function Requests({ requests=[] }) {
  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={requests}
        renderItem={(item) => <RequestCard {...item} />}
        keyExtractor={(item) => item.id.toString()}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
