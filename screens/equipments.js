import React from "react";
import { FlatList, SafeAreaView, StyleSheet } from "react-native";

import EquipmentCard from "../components/cards/equipmentCard";

import { data } from "../mockData/Data.json";

export default function EquipmentsList() {
  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={data.filter((ele) => ele["system.name"] === "Equipment")}
        renderItem={(item) => <EquipmentCard {...item} />}
        keyExtractor={(item) => item["system.id"].toString()}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
