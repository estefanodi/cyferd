# Description 

The app is built with React Native and Expo, it is divided in 2 parts, equipments and manufactors, from a main screen you can access both of them

<img src="https://res.cloudinary.com/estefanodi2009/image/upload/v1628797130/WhatsApp_Image_2021-08-12_at_21.35.32_5.jpg" width='300px' alt="main screen">

## The manufacturer part is not done

<img src="https://res.cloudinary.com/estefanodi2009/image/upload/v1628797130/WhatsApp_Image_2021-08-12_at_21.35.32_6.jpg" width='300px' alt="main screen">



## Inside equipments there are 3 main tabs:

## A form to create equipments requests



<img src="https://res.cloudinary.com/estefanodi2009/image/upload/v1628797131/WhatsApp_Image_2021-08-12_at_21.35.32.jpg" width='300px' alt="main screen">



## a screen where to check al the available equipments 



<img src="https://res.cloudinary.com/estefanodi2009/image/upload/v1628797130/WhatsApp_Image_2021-08-12_at_21.35.32_3.jpg" width='300px' alt="main screen">



## and a screen where to check all the equipment requests



<img src="https://res.cloudinary.com/estefanodi2009/image/upload/v1628797130/WhatsApp_Image_2021-08-12_at_21.35.32_2.jpg" width='300px' alt="main screen">


For the navigation I used React Navigation.

# How to run it 1

If you don't have expo installed:

```
npm i expo-cli -g
git clone https://gitlab.com/estefanodi/cyferd
cd cyferd
npm i
npm start
```

If you have expo installed in your laptop:

```
git clone https://gitlab.com/estefanodi/cyferd
cd cyferd
npm i
npm start
```

After this the terminal will give you a qr code, either from ios or android you'll need to install
expo-go on your phone (or use a simulator).

If you are using android you can scan the qr code from expo-go, if you are using ios you can scan the qr code from the camera and it will open the project on expo-go.

# How to run it 2

Install expo-go on your phone and scan the qr code from this page:

[expo qr code](https://expo.dev/@estefanodi2020/test-cyferd)
