import React from "react";

import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { HeaderBackButton } from "@react-navigation/elements";

import EquipmentRequest from "../screens/equipmentRequest";
import Equipments from "../screens/equipments";
import Requests from "../screens/requests";

const BottomTabs = createBottomTabNavigator();

const getTabBarIcon =
  (name) =>
  ({ color, size }) =>
    <MaterialCommunityIcons name={name} color={color} size={size} />;

export default function EquipmentsRoutes({
  createRequest,
  requests,
  setScreen,
}) {
  return (
    <BottomTabs.Navigator
      initialRouteName="EquipmentRequest"
      screenOptions={{
        headerLeft: (props) => {
          return (
            <HeaderBackButton
              {...props}
              onPress={() => {
                setScreen("main");
              }}
            />
          );
        },
      }}
    >
      <BottomTabs.Screen
        name="EquipmentRequest"
        options={{
          title: "Equipment Request",
          tabBarIcon: getTabBarIcon("file-document"),
        }}
      >
        {(props) => (
          <EquipmentRequest
            {...props}
            appIdx={0}
            initialData={equipmentRequestInitialData}
            handleSubmit={createRequest}
          />
        )}
      </BottomTabs.Screen>
      <BottomTabs.Screen
        name="Equipments"
        options={{
          tabBarLabel: "Equipments",
          tabBarIcon: getTabBarIcon("format-list-bulleted"),
        }}
      >
        {(props) => <Equipments {...props} />}
      </BottomTabs.Screen>
      <BottomTabs.Screen
        name="Requests"
        options={{
          tabBarLabel: "Requests",
          tabBarIcon: getTabBarIcon("email-outline"),
        }}
      >
        {(props) => <Requests {...props} requests={requests} />}
      </BottomTabs.Screen>
    </BottomTabs.Navigator>
  );
}

const equipmentRequestInitialData = {
  "Request Date/Time": new Date(),
  "Requested By": "",
  Quantity: 1,
  Equipment: null,
};
