import React from "react";
import { View, Text, StyleSheet } from "react-native";

import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { HeaderBackButton } from "@react-navigation/elements";

const BottomTabs = createBottomTabNavigator();

const getTabBarIcon =
  (name) =>
  ({ color, size }) =>
    <MaterialCommunityIcons name={name} color={color} size={size} />;

const UnderConstruction = () => (
  <View style={styles.container}>
    <Text>Under Construction</Text>
  </View>
);

export default function ManufacturersRoutes({ setScreen }) {
  return (
    <BottomTabs.Navigator
      initialRouteName="UnderConstruction"
      screenOptions={{
        headerLeft: (props) => {
          return (
            <HeaderBackButton {...props} onPress={() => setScreen("main")} />
          );
        },
      }}
    >
      <BottomTabs.Screen
        name="UnderConstruction"
        component={UnderConstruction}
        options={{
          title: "Under Construction",
          tabBarIcon: getTabBarIcon("file-document"),
        }}
      />
    </BottomTabs.Navigator>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
});
