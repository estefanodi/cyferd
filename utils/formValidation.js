import { apps } from "../mockData/Apps.json";

//! THIS FUNCTION SHOULD BE IMPROVED

export const validateEquipmentRequest = (data, appIdx) => {
  for (let key in data) {
    const rules = apps[appIdx].fields.find(
      (ele) => ele["name"] === key
    ).validation;
    if (
      (typeof data[key] === "number" && data[key] > rules["max-value"]) ||
      data[key] < rules["min-value"]
    )
      return [false, "Wrong quantity range"];
    if (rules.required && !data[key])
      return [false, `The field ${key} is required`];
    if (key === "Request Date/Time" && new Date() > data[key])
      return [false, `The date must be a future date`];
  }
  return [true, "Request created"];
};
