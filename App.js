import React from "react";
import Constants from "expo-constants";
import { NavigationContainer } from "@react-navigation/native";
import { StyleSheet, View } from "react-native";

import EquipmentsRoutes from "./navigation/equipmentsRoutes";
import ManufacturersRoutes from "./navigation/manufacturersRoutes";

import Main from "./screens/main";

export default function App() {
  const [requests, setRequests] = React.useState([...mockRequests]);
  const [stack, setStack] = React.useState(null);
  const [screen, setScreen] = React.useState("main");
  const [requestId, setRequestId] = React.useState(2);

  //* =========================================================
  //* ==================  CREATE REQUEST ======================
  //* =========================================================
  const createRequest = (newRequest, navigation) => {
    const temp = [...requests];
    //! CHECK THE STOCK BEFORE REDUCE 
    newRequest.id = requestId;
    setRequestId((requestId) => (requestId += 1));
    temp.push(newRequest);
    setRequests(temp);
    navigation.navigate('Requests')
  };
  //* =========================================================
  //* ===========  selector from main.js ======================
  //* =========================================================
  const stackSelector = {
    equipments: (
      <EquipmentsRoutes
        requests={requests}
        createRequest={createRequest}
        setScreen={setScreen}
      />
    ),
    manufacturers: <ManufacturersRoutes setScreen={setScreen} />,
  };
  //* =========================================================
  //* == switch between main screen and the rest of the app  ==
  //* =========================================================
  const screenSelector = {
    main: <Main setStack={setStack} setScreen={setScreen} />,
    navigation: (
      <NavigationContainer>{stackSelector[stack]}</NavigationContainer>
    ),
  };
  //* =========================================================
  //* ========================  RENDER  =======================
  //* =========================================================
  return <View style={styles.container}>{screenSelector[screen]}</View>;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: Constants.statusBarHeight,
  },
});

const mockRequests = [
  {
    "Request Date/Time": new Date(),
    "Requested By": "mario",
    Quantity: 2,
    Equipment: "Laptop",
    id: 0
  },
  {
    "Request Date/Time": new Date(),
    "Requested By": "antony",
    Quantity: 1,
    Equipment: "PC",
    id: 1
  },
];
