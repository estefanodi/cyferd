import React from "react";
import { View, StyleSheet, Dimensions, Text, Image } from "react-native";
import moment from "moment";

import Row from "./row";

import { data } from "../../mockData/Data.json";

const { height, width } = Dimensions.get("window");

const CardHeader = ({ equipment }) => (
  <View style={styles.cardHeader}>
    <Image
      style={styles.image}
      source={{ uri: data.find((ele) => ele.Name === equipment).Image }}
    />
  </View>
);

export default function RequestCard({ item }) {
  return (
    <View style={styles.cardContainer}>
      <CardHeader equipment={item.Equipment} date={item["Request Date/Time"]} />
      <Row type={"Request id"} value={item["id"]} rowHeight={"16%"} />
      <Row
        type={"Requested By"}
        value={item["Requested By"]}
        rowHeight={"16%"}
      />
      <Row type={"Quantity"} value={item["Quantity"]} rowHeight={"16%"} />
      <Row
        type={"Request Date/Time"}
        value={moment(item["Request Date/Time"]).format("L")}
        rowHeight={"16%"}
      />
      <Row type={"Equipment"} value={item["Equipment"]} rowHeight={"16%"} />
    </View>
  );
}

const styles = StyleSheet.create({
  cardContainer: {
    width: "90%",
    height: height / 2,
    flexDirection: "column",
    borderColor: "gray",
    borderWidth: 1,
    marginTop: 20,
    marginBottom: 20,
    marginLeft: "5%",
    borderTopLeftRadius: 7,
    borderTopRightRadius: 7,
    borderBottomLeftRadius: 7,
    borderBottomRightRadius: 7,
  },
  cardHeader: {
    height: "20%",
    borderColor: "gray",
    borderWidth: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-end",
  },
  image: {
    width: "100%",
    height: "100%",
    resizeMode: "contain",
  },
  row: {
    width: width / 1.1,
    height: "10%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: 20,
  },
});
