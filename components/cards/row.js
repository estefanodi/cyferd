import React from "react";
import { View, StyleSheet, Dimensions, Text } from "react-native";

const { width } = Dimensions.get("window");

export default function Row({ type, value, rowHeight }) {
  return (
    <View style={[styles.row,{height: rowHeight}]}>
      <Text style={styles.key}>{type}</Text>
      <Text>{value}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  row: {
    width: width / 1.1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: 20,
  },
  key: {
    fontWeight: "700",
  },
});
