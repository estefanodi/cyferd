import React from "react";
import { View, StyleSheet, Dimensions, Text, Image } from "react-native";

import Row from "./row";

import { data } from "../../mockData/Data.json";

const { height, width } = Dimensions.get("window");

const getManufacturer = (str) => {
  const manufacturerId = Number(
    str.replace("{", "").replace("}", "").split(":")[1]
  );
  return data.find(
    (ele) =>
      ele["system.name"] === "Manufacturer" &&
      ele["system.id"] === manufacturerId
  ).Name;
};

export default function EquipmentCard({ item }) {
  return (
    <View style={styles.cardContainer}>
      <View style={styles.titleContainer}>
        <Text style={styles.title}>{item.Name}</Text>
      </View>

      <View style={styles.imageContainer}>
        <Image style={styles.image} source={{ uri: item.Image }} />
      </View>

      <Row type={"Price"} value={item.Price} rowHeight={"10%"} />
      <Row
        type={"Manufacturer"}
        value={getManufacturer(item.Manufacturer)}
        rowHeight={"10%"}
      />
      <Row type={"Stock"} value={item.Stock} rowHeight={"10%"} />
    </View>
  );
}

const styles = StyleSheet.create({
  cardContainer: {
    width: "90%",
    height: height / 2,
    flexDirection: "column",
    borderColor: "gray",
    borderWidth: 1,
    marginTop: 20,
    marginBottom: 20,
    marginLeft: "5%",
    borderTopLeftRadius: 7,
    borderTopRightRadius: 7,
    borderBottomLeftRadius: 7,
    borderBottomRightRadius: 7,
  },
  titleContainer: {
    height: "15%",
    backgroundColor: "black",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    borderTopLeftRadius: 7,
    borderTopRightRadius: 7,
  },
  title: {
    color: "white",
  },
  imageContainer: {
    height: "55%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  image: {
    width: width / 2,
    height: "100%",
    resizeMode: "contain",
  },
});
