import React, { useState } from "react";
import { View, Platform, Dimensions, StyleSheet } from "react-native";
import DateTimePicker from "@react-native-community/datetimepicker";
import moment from "moment";

import Button from "./button";

const { width } = Dimensions.get("window");

export default function DatePicker({ length, data, setData }) {
  const [show, setShow] = useState(false);

  const onChange = (_, selectedDate) => {
    const currentDate = selectedDate || new Date();
    setShow(Platform.OS === "ios");
    setData({ ...data, "Request Date/Time": currentDate });
  };

  return (
    <View style={[styles.pickerContainer, { width: width / length }]}>
      {show && (
        <DateTimePicker
          testID="dateTimePicker"
          value={data["Request Date/Time"]}
          mode={"date"}
          is24Hour={true}
          display="spinner"
          onChange={onChange}
        />
      )}
      <Button
        action={() => setShow(true)}
        title={moment(data["Request Date/Time"]).format("L")}
        length={length}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  pickerContainer: {
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
  button: {
    width: "90%",
    height: "60%",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "gray",
    borderTopLeftRadius: 7,
    borderTopRightRadius: 7,
    borderBottomLeftRadius: 7,
    borderBottomRightRadius: 7,
  },
  buttonTitle: {
    color: "white",
    letterSpacing: 2,
    fontWeight: "700",
  },
});
