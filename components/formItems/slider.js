import React from "react";
import { View, Text, StyleSheet, Dimensions } from "react-native";
import Slider from "@react-native-community/slider";

const { width } = Dimensions.get("window");

export default function SliderComponent({
  properties,
  length,
  placeholder,
  data,
  setData,
  type,
}) {
  return (
    <View style={[styles.sliderContainer, { width: width / length }]}>
      <Text>{placeholder}</Text>
      <Slider
        style={styles.slider}
        minimumValue={properties["min-value"]}
        maximumValue={properties["max-value"]}
        thumbTintColor={"#0170fe"}
        minimumTrackTintColor="#0170fe"
        maximumTrackTintColor="#000000"
        step={properties["step"]}
        value={data[type]}
        onValueChange={(value) => setData({ ...data, Quantity: value })}
      />
      <Text>{data[type]}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  sliderContainer: {
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
  },
  slider: {
    width: "60%",
    borderColor: "gray",
    borderWidth: 1,
  },
});
