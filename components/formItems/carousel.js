import React from "react";
import {
  ScrollView,
  StyleSheet,
  Dimensions,
  Image,
  TouchableOpacity,
} from "react-native";

import { data as jsonData } from "../../mockData/Data.json";

const { width } = Dimensions.get("window");

export default function CarouselComponent({ data, setData }) {
  const [selectedIdx, setSelectedIdx] = React.useState(null);
  return (
    <ScrollView style={[styles.carousel]} horizontal={true}>
      {jsonData
        .filter((ele) => ele["system.name"] === "Equipment")
        .map((ele, idx) => (
          <TouchableOpacity
            key={ele["system.id"]}
            style={[
              styles.imageContainer,
              { borderWidth: idx === selectedIdx ? 3 : 1 },
            ]}
            onPress={() => {
              setData({ ...data, Equipment: ele.Name });
              setSelectedIdx(idx);
            }}
          >
            <Image style={styles.image} source={{ uri: ele.Image }} />
          </TouchableOpacity>
        ))}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  carousel: {
    width,
    flexDirection: "row",
  },
  imageContainer: {
    width: width / 4,
    padding: 10,
    marginHorizontal: 5,
  },
  image: {
    width: "100%",
    height: "100%",
    resizeMode: "contain",
  },
});
