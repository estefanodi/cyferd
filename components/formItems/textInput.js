import React from "react";
import { View, TextInput, StyleSheet, Dimensions } from "react-native";

const { width } = Dimensions.get("window");

export default function TextInputComponent({
  placeholder,
  length,
  data,
  setData,
}) {
  return (
    <View style={[styles.inputContainer, { width: width / length }]}>
      <TextInput
        style={styles.input}
        placeholder={placeholder}
        onChangeText={(value) => setData({ ...data, "Requested By": value })}
        value={data["Requested By"]}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  inputContainer: {
    height: "100%",
    justifyContent: "center",
    paddingHorizontal: 10,
  },
  input: {
    height: "60%",
    borderColor: "gray",
    borderWidth: 1,
    paddingLeft: 20,
  },
});
